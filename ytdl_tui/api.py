"""Class to interact with the ytdl-api-server
"""

from attrs import define
from dotenv import load_dotenv
import requests
from rich import print

from os import getenv

load_dotenv()


@define
class YTDLApi:
    base_url: str = getenv("BASE_URL", None)
    debug: bool = False

    def get_pending(self):
        res = requests.get(f"{self.base_url}/urls/pending")
        res.raise_for_status()
        if self.debug:
            print("Pending items API result:")
            print(res.json())
        return {int(k): v for (k, v) in res.json().items()}

    def add(self, url):
        res = requests.post(f"{self.base_url}/urls/add", json={"url": url})
        res.raise_for_status()

    def mark_downloaded(self, entry_id):
        res = requests.put(f"{self.base_url}/urls/downloaded", json={"id": entry_id})
        res.raise_for_status()

    def delete(self, entry_id):
        res = requests.delete(f"{self.base_url}/urls/delete", json={"id": entry_id})
        res.raise_for_status()


if __name__ == "__main__":
    api = YTDLApi(debug=True)
    # print(api.get_pending())
    # print("Adding")
    # api.add("https://www.youtube.com/watch?v=WTX3JtGcJZ4")
    # pending = api.get_pending()
    # print(pending)
    # print("Marking first item downloaded")
    # api.mark_downloaded(entry_id=list(pending.keys())[0])
    # print(api.get_pending())
    # print("Deleting last item")
    # api.delete(entry_id=list(pending.keys())[-1])
    # print(api.get_pending())
    pending = api.get_pending()
    print(pending)
