from attrs import define
from textual.app import App, ComposeResult
from textual.containers import Container, Grid
from textual.reactive import reactive, var
from textual.screen import Screen
from textual.widgets import Header, Footer, Static, Button, Input

from api import YTDLApi

import itertools

API = YTDLApi()


class Entry(Static):

    highlight = var(False)

    def __init__(self, _id, url):
        super().__init__()
        self._id = _id
        self.url = url

    def compose(self) -> ComposeResult:
        yield Static(str(self._id), id="entry_id")
        yield Static(str(self.url), id="entry_url")

    def watch_highlight(self, new_val):
        if new_val:
            self.add_class("highlighted")
        else:
            self.remove_class("highlighted")


class AddScreen(Screen):
    def compose(self) -> ComposeResult:
        yield Grid(
            Input(placeholder="URL to download", id="question"),
            Button("Add", variant="primary", id="add"),
            Button("Cancel", variant="cancel", id="cancel"),
            id="dialog",
        )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "add":
            API.add(self.query_one("#question", Input).value)
        self.app.pop_screen()


class BaseScreen(Screen):
    BINDINGS = [
        ("a", "add_url", "Add URL for download"),
        ("r", "refresh", "Refresh"),
        ("d", "delete", "Delete URL"),
    ]
    entries = var([])
    highlighted = var(0)

    def compose(self) -> ComposeResult:
        """Create child widgets for the app."""
        yield Header()
        yield Footer()

    def on_screen_resume(self):
        self.get_entries()

    def on_mount(self):
        self.get_entries()

    def get_entries(self):
        pending = API.get_pending()
        for (n, entry) in enumerate(self.entries):
            if entry._id not in pending.keys():
                self.entries.remove(entry)
                if entry.highlight:
                    j = n - 1
                    if (j < 0) or (len(self.entries) < j):
                        j = 0
                    if self.entries:
                        self.entries[j].highlight = True
                        self.highlighted = j
                entry.remove()

        ids = [e._id for e in self.entries]
        for (_id, url) in pending.items():
            if _id not in ids:
                entry = Entry(_id, url)
                if not self.entries:
                    entry.highlight = True
                    self.highlighted = 0
                self.mount(entry)
                self.entries.append(entry)

    def move_highlight(self, direction) -> None:
        self.entries[self.highlighted].highlight = False
        self.highlighted = self.highlighted + direction
        if self.highlighted >= len(self.entries):
            self.highlighted = 0
        elif self.highlighted < 0:
            self.highlighted = len(self.entries) - 1
        self.entries[self.highlighted].highlight = True

    def action_add_url(self) -> None:
        self.app.push_screen(AddScreen())

    def action_refresh(self) -> None:
        self.get_entries()

    def action_delete(self) -> None:
        API.delete(self.entries[self.highlighted]._id)
        self.get_entries()

    def key_up(self):
        self.move_highlight(direction=-1)

    def key_down(self):
        self.move_highlight(direction=1)


class YTDLApp(App):
    CSS_PATH = "main.css"
    SCREENS = {"base": BaseScreen(), "add": AddScreen()}
    BINDINGS = [
        ("d", "toggle_dark", "Toggle dark mode"),
    ]

    def on_mount(self) -> None:
        self.push_screen("base")

    def action_toggle_dark(self) -> None:
        """An action to toggle dark mode."""
        self.dark = not self.dark


if __name__ == "__main__":
    app = YTDLApp()
    app.run()
